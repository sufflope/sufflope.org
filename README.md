```shell
dnf distro-sync
hostnamectl hostname sufflope.org
dnf install git iscsi-initiator-utils podman
mkdir -p /etc/rancher/k3s
git clone https://gitlab.com/sufflope/sufflope.org.git
cp sufflope.org/bootstrap/config.yaml /etc/rancher/k3s/config.yaml
curl -sfL https://get.k3s.io | INSTALL_K3S_CHANNEL=latest sh -s -
kubectl apply -f sufflope.org/bootstrap/argocd.yaml
for i in postfix postgrey; do podman build -t git.sufflope.org/sufflope/$i:2023112701 . -f sufflope.org/images/Dockerfile.$i; podman save git.sufflope.org/sufflope/$i:2023112701 | k3s ctr image import -; done
kubectl apply -f https://gitlab.com/sufflope/sufflope.org/-/raw/main/bootstrap/root.yaml
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```
